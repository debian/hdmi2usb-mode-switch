Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: HDMI2USB-mode-switch
Source: https://github.com/timvideos/HDMI2USB-mode-switch

Files: *
Copyright: 2015-2016, Tim 'mithro' Ansell <mithro@mithis.com>
License: Apache-2.0

Files: hdmi2usb/firmware/fx2/*
Copyright: 2003-2005, Free Software Foundation, Inc,
           2005-2007, Kolja Waschk, ixo.de,
           2015-2016, Tim 'mithro' Ansell <mithro@mithis.com>
License: GPL-2+
Comment: The source is in the ixo-usb-jtag and hdmi2usb-fx2-firmware packages.

Files: hdmi2usb/firmware/spartan6/*
       xilinx_bscan_spi.py
Copyright: 2015, Robert Jordens <jordens@gmail.com>
License: GPL-2+
Comment: The source will be in the openocd package, but isn't yet, as of 0.9.0.
 So, included as xilinx_bscan_spi.py
 Migen itself isn't currently in Debian, but is Free Software. However,
 synthesising the FPGA bitstream requires a non-free toolchain.

Files: debian/*
Copyright: 2016-2022, Stefano Rivera <stefanor@debian.org>
License: GPL-2+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License.  You may obtain a copy of
 the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 License for the specific language governing permissions and limitations under
 the License.
 .
 On Debian systems, the complete text of the Apache License, Version 2.0 can be
 found in "/usr/share/common-licenses/Apache-2.0".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
