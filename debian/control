Source: hdmi2usb-mode-switch
Section: admin
Priority: optional
Maintainer: Stefano Rivera <stefanor@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 dh-python,
 pkgconf,
 python3,
 python3-setuptools,
 systemd-dev,
Standards-Version: 4.6.2
Homepage: https://hdmi2usb.tv/
Vcs-Git: https://salsa.debian.org/debian/hdmi2usb-mode-switch.git
Vcs-Browser: https://salsa.debian.org/debian/hdmi2usb-mode-switch
Rules-Requires-Root: no

Package: hdmi2usb-mode-switch
Architecture: all
Depends:
 fxload,
 hdmi2usb-fx2-firmware,
 hdmi2usb-udev,
 ixo-usb-jtag,
 openocd,
 python3-pkg-resources,
 usbutils,
 ${misc:Depends},
 ${python3:Depends}
Description: Configuration and firmware tool for HDMI2USB devices
 This is the tool for flashing and configuring the HDMI2USB devices.
 .
 It can load a runtime firmware, and write firmware to the device's flash.
 .
 https://hdmi2usb.tv/ is an open hardware and software project for capturing
 HDMI video with an FPGA board. This package supports the Digilent Atlys and
 Numato Opsis boards.

Package: hdmi2usb-udev
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: udev rules for HDMI2USB devices
 This package provides a set of udev rules files for HDMI2USB devices. They
 grant access to users in the "video" group, provide stable device symlinks,
 and suppress ModemManager.
 .
 https://hdmi2usb.tv/ is an open hardware and software project for capturing
 HDMI video with an FPGA board. This package supports the Digilent Atlys and
 Numato Opsis boards.
